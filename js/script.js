const palabras = document.querySelectorAll('.lista-palabras li');
const contenedorResultado = document.querySelector('.resultado');

function capitaliza() {
  const palabrasResultado = contenedorResultado.querySelectorAll('li');
  let palabraTexto = palabrasResultado[0].textContent;
  palabrasResultado[0].textContent = palabraTexto.charAt(0).toUpperCase() + palabraTexto.slice(1);
}

function eliminarPalabra(palabra) {
  palabra.remove();
  capitaliza();
}

for (let palabra of palabras) {
  palabra.addEventListener('click', () => {
    const palabraNueva = palabra.cloneNode(true);
    palabraNueva.classList.add('palabra-nueva');
    palabraNueva.addEventListener('click', () => {
      eliminarPalabra(palabraNueva);
    });
    contenedorResultado.append(palabraNueva);
    capitaliza();
  });
}

